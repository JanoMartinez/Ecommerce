'use strict';
angular.module('softvFrostApp').controller('LoginCtrl', LoginCtrl);

function LoginCtrl(authFactory, pagoReciboFactory, ngNotify, $state, $localStorage, $stateParams, $window, $location) {
	var vm = this;
	vm.login = login;
	vm.linkRegistro = '';
	vm.mostrarPagina = false;
	vm.contrato = 0;
	
	this.$onInit = function () {		
		vm.mostrarPagina = checaNavegador();	
		vm.contrato = 0;	
		
		if (vm.mostrarPagina == true){
			//Revisar si existen las variables en el navegador	on 
			//redireccion(); // descomentar para logueo
			$state.go('home.pagoEnLinea.pago');
			/*if ($localStorage.currentUser) {
				if ($stateParams.esn != undefined) {
					$state.go('home.provision.activacion', {
						'esn': $stateParams.esn
					});
				} else {					
					$state.go('home.pagoEnLinea.pago');			
				}
			}*/		
		}	
	}



	function login() {
		authFactory.login(vm.user, vm.password).then(function (data) {
			$localStorage.currentPay.logueoAutomatico = 0;	
			if (data) {
				$window.location.reload();
			} else {
				ngNotify.set('Datos de acceso erróneos', 'error');
			}
		});
	}


	function loginAutomatico(login, pass, result_success) {
		authFactory.login(login, pass).then(function (data) {
		
			$localStorage.currentPay.resultIndicator = result_success; // checar			
			$localStorage.currentPay.successIndicator = result_success;
			$localStorage.currentPay.logueoAutomatico = 1;
			$localStorage.currentUser.contrato = vm.contrato;

			console.log('$localStorage.currentPay.', $localStorage.currentPay); 

			if (data) {
								
				cambiarUrl(); //$window.location.reload();
			
			} else {
				ngNotify.set('Datos de acceso erróneos', 'error');
			}
		});
	}



    function redireccion(){
        	
            var urlCancelado =  window.location.href;          
            var n = urlCancelado.indexOf("resultIndicator");          
 
            if (n == -1){
                // No existe resultIndicator
               dameLinkRegistro(); //link de registro stargo 
            }
            else {              
                // Redirección por PAGO hecho
              
                    // Get variable from url
                    var urlRedire =  window.location.href; 
                    var url = new URL(urlRedire);
                    var result_success =  url.searchParams.get("resultIndicator");              
                    var sV =  url.searchParams.get("sessionVersion");                         
                    var loginUsuarioAutomatico = 0;         
					var pasaporteUsuarioAutomatico = 0; 

					authFactory.datosLogueoDelContrato(result_success, 1, urlCancelado).then(function(data) { 							      
				        loginUsuarioAutomatico = data.GetDatosLogueoResult[0].Login;
				        pasaporteUsuarioAutomatico = data.GetDatosLogueoResult[0].Pasaporte;	
				       	vm.contrato = data.GetDatosLogueoResult[0].Contrato;					      
				        loginAutomatico(loginUsuarioAutomatico, pasaporteUsuarioAutomatico, result_success);			    
				    });  
				    
            }       	     
     }


    function cambiarUrl(){         

        var urlResult =  window.location.href;  
        var afterComma = urlResult.substr(urlResult.indexOf("?")); // Contains 24 //     
        var quitarDeUrl = afterComma.substring(0, afterComma.indexOf("#"));            
        location.href = location.href.replace(quitarDeUrl, ''); 
	
    }


	function dameLinkRegistro() {
		authFactory.GetLinkRegistro().then(function(data) { 

			vm.linkRegistro = data.GetLinkRegistroResult[0].linkRegistro	
			vm.linkPassword = data.GetLinkRegistroResult[0].linkRecuperaPassword	
					
		}); 
		
	}



	function checaNavegador(){
		// Opera 8.0+
		var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;

		// Firefox 1.0+
		var isFirefox = typeof InstallTrigger !== 'undefined';

		// Safari 3.0+ "[object HTMLElementConstructor]" 
		var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));

		// Internet Explorer 6-11
		var isIE = /*@cc_on!@*/false || !!document.documentMode;

		// Edge 20+
		var isEdge = !isIE && !!window.StyleMedia;

		// Chrome 1+
		var isChrome = !!window.chrome && !!window.chrome.webstore;

		// Blink engine detection
		var isBlink = (isChrome || isOpera) && !!window.CSS;

		//console.log('isOpera ', isOpera, ', isFirefox ', isFirefox, 'isSafari', isSafari, 'isIE', isIE, 'isEdge', isEdge, 'isChrome', isChrome, 'isBlink', isBlink);

		var mostrarPago = false;

		if ((isIE == true)||(isEdge == true)){
			mostrarPago = false;
		}
		else{
			mostrarPago = true;
		}

		return mostrarPago;
	}


}
