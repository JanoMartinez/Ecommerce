'use strict';
angular.module('softvFrostApp')
    .controller('PagoCanceladoCtrl', function ($uibModal, $state, $rootScope, ngNotify, $localStorage, $window, pagoEnLineaFactory, pagoReciboFactory,  globalService, $sce,  $http) // servicio
    {
        var vm = this;  
        var ContratoComp;
        var ContratoReal;
        var NombreCliente;
   
      //  vm.openPay = openPay;

        function logOut() {      
            delete $localStorage.currentUser;
            $window.location.reload();
        }

        this.$onInit = function () {

            var urlCancelado = window.location.href;
            pagoReciboFactory.guardaRedireccion(5, $localStorage.currentUser.contrato, $localStorage.currentPay.clvSessionCobra, $localStorage.currentPay.idSession, urlCancelado).then(function (data) {                     
            });             

            setTimeout( function() {
                ngNotify.set('El pago ha sido cancelado.', 'error');
                logOut();      
            }, 2500);        
            
        }       
    });
