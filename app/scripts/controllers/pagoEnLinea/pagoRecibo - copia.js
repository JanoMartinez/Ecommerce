﻿'use strict';
angular.module('softvFrostApp')
    .controller('PagoReciboCtrl', function ($uibModal, $state, $rootScope, ngNotify, $localStorage, $window, pagoReciboFactory, globalService, $sce, $http) // servicio
    {
        var vm = this;
        var ContratoComp;
        var ContratoReal;
        var NombreCliente;


        function logOut() {
            delete $localStorage.currentUser;
            $window.location.reload();
        }


        this.$onInit = function () {   

             // Guardamos en bitácora los primeros datos que regresa el banco 
            var url1 = ''; 
            url1 = window.location.href;
            
            // tomar en cuenta que se puede guardar solo la url
            pagoReciboFactory.guardaRedireccion(11, $localStorage.currentUser.contrato, $localStorage.currentPay.clvSessionCobra, $localStorage.currentPay.idSession, url1).then(function (data) {
            console.log('11 redireccion guardada ***', data);
            });

            // Obtenemos los datos del merchant
            
            pagoReciboFactory.datosMerchant().then(function (data) {
                $localStorage.merchantData.merchantId = data.GetDatosMerchantResult[0].merchantId;
                $localStorage.merchantData.userId = data.GetDatosMerchantResult[0].userId;
                $localStorage.merchantData.password = data.GetDatosMerchantResult[0].password;
                $localStorage.merchantData.merchantName = data.GetDatosMerchantResult[0].merchantName;
                $localStorage.merchantData.addressLine1 = data.GetDatosMerchantResult[0].addressLine1;
                $localStorage.merchantData.addressLine2 = data.GetDatosMerchantResult[0].addressLine2;
                $localStorage.merchantData.email = data.GetDatosMerchantResult[0].email;
                $localStorage.merchantData.descripcionImporte = data.GetDatosMerchantResult[0].descripcionImporte;

                console.log(' $localStorage.merchantData',  $localStorage.merchantData);
            });
        

            if ($localStorage.currentPay.logueoAutomatico == 1) {
                console.log('fue logueo automático');
                // Obtenemos idTransaccion
                pagoReciboFactory.getIdSessionTransaccion($localStorage.currentUser.contrato, $localStorage.currentPay.resultIndicator).then(function (data) {

                    $localStorage.currentPay.idSession = data.GetIdSessionTransaccionResult[0].idSession; // Traemos el id sesion del pago guardado
                    $localStorage.currentPay.clvSessionCobra = data.GetIdSessionTransaccionResult[0].clv_session; //Traemos la clv sesion del pago guardada
                                                 
                }, 1200);
            }

            console.log('getIdSessionTransaccion *- $localStorage.currentUser.contrato: ', $localStorage.currentUser.contrato );
            // '* $localStorage.currentPay.resultIndicator: ', $localStorage.currentPay.resultIndicator
           

            // Revisamos las variables del navegador 
            var urlnavegador = window.location.href;
            var n = urlnavegador.indexOf("resultIndicator");
            var url = new URL(urlCancelado);            
                             
            if (n == -1) {
            // No existe resultIndicator en URL  
                        pagoReciboFactory.guardaRedireccion(2, $localStorage.currentUser.contrato, $localStorage.currentPay.clvSessionCobra, $localStorage.currentPay.idSession, urlnavegador).then(function (data) {
                        //    console.log('redireccion guardada ***',nivel,' - ', data);
                        });                                                           
            }
            else {
                    // INICIA GUARDAR PAGO 
                    // Redirección por pago, si tiene resultIndicator, se verifica el pago en la bd
                    pagoReciboFactory.guardaRedireccion(1, $localStorage.currentUser.contrato, $localStorage.currentPay.clvSessionCobra, $localStorage.currentPay.idSession, urlnavegador).then(function (data) {
            
                    }); 
                    // TERMINA GUARDAR PAGO                           
                   
                if ($localStorage.currentPay.idSession != null) //ID SESSION DEL CHECKOUT
                {
                    var urlRedire = window.location.href;
                    var url = new URL(urlRedire);
                    $localStorage.currentPay.resultIndicator = url.searchParams.get("resultIndicator");
                    $localStorage.currentPay.sessionVersion = url.searchParams.get("sessionVersion"); 
                }

            } // fin else  


          
            //Inicia Exec_Retrieve  
                var idSessionCobra = $localStorage.currentPay.clvSessionCobra; 
            
                console.log(' $localStorage.currentPay.resultIndicator: ',  $localStorage.currentPay.resultIndicator);
                console.log('$localStorage.currentPay.sessionVersion: ', $localStorage.currentPay.sessionVersion ); 
                console.log('idSessionCobra: ', idSessionCobra); 

                // EXTRA, PARA OBTENER EL OBJETO DEL PAGO 
                pagoReciboFactory.banamexRetrieve(
                    $localStorage.merchantData.password,
                    $localStorage.merchantData.userId,
                    $localStorage.currentPay.idSession,
                    $localStorage.merchantData.merchantId,
                    idSessionCobra).then(function (data) {  

                    vm.objRetrieve = JSON.parse(data.GetRetrieveResult);
                    var retrieveData = data.GetRetrieveResult;

                    // Guarda el error del objeto 
                    if (vm.objRetrieve.hasOwnProperty('error')) {
                        ngNotify.set('No es posible generar su recibo en este momento. ' + vm.objRetrieve.error.cause, 'error');                      
                        guardarError(retrieveData);                                            
                    }

                    

                    var successIndicator = $localStorage.currentPay.successIndicator;
                    var valResultIndicator = $localStorage.currentPay.resultIndicator;
                    var orderId = idSessionCobra; //ultima session cobra del pago    
                   
                    /*else{
                        orderId = id; //la que procede del banco
                    }*/    
                                    
                   /*
                    console.log('antes de guardar el return data');
                    console.log('orderId', orderId);
                    console.log('successIndicator', successIndicator);
                    console.log('valResultIndicator', valResultIndicator);

                    // guarda return data
                    setTimeout( function() { 
                    //nota: el 0 representa SIN ERROR
                        pagoReciboFactory.guardaReturnData(0, orderId, retrieveData, valResultIndicator, 
                        '','','','','' // por el momento lo enviamos en blanco 
                        //amount, description, id, brand, transactionId // variables para enviar el recibo
                        ).then(function (data) {
                        
                        console.log('GUARDA RETURN DATA');
                        console.log('orderId: ', orderId);
                        console.log('retrieveData: ', retrieveData);
                        console.log('valResultIndicator:' ,valResultIndicator);
                        console.log('data ', data);
                            //llamaba al modal 
                        });
                    },900);
                      */
                });//Fin exec retrieve

              

                setTimeout( function() { cambiarUrl()}, 5000);
                //setTimeout( function() { logOut()}, 5000);
        }



        function guardarError(retrieveData){
                                     
                //pagoReciboFactory.guardaRedireccion(9, $localStorage.currentUser.contrato, $localStorage.currentPay.clvSessionCobra, $localStorage.currentPay.idSession, '').then(function (data) {
                    //console.log('guardaReturndata del error ',$localStorage.currentPay.clvSessionCobra, retrieveData, $localStorage.currentPay.resultIndicator, '', '', '', '', '');
                    pagoReciboFactory.guardaReturnData(1 ,$localStorage.currentPay.clvSessionCobra , retrieveData  , $localStorage.currentPay.resultIndicator,'', '', '', '', '').then(function (data) {                                                                                           
                    });         
                //});                 
            
            
            setTimeout( function() { logOut()}, 5000);
        }


        function logOut() {           
           delete $localStorage.currentUser;
           $window.location.reload();
        }

        function limpiarUrl(){
            console.log('limpiamos url');
           //$location.url($location.path());   
          //  $state.go('home.pagoEnLinea.pago'); 
        }


        function cambiarUrl() {
            $localStorage.currentPay.urlCambiada = 1;

            var urlResult = window.location.href;
            var afterComma = urlResult.substr(urlResult.indexOf("?")); // Contains 24 //     
            var quitarDeUrl = afterComma.substring(0, afterComma.indexOf("#"));
            location.href = location.href.replace(quitarDeUrl, '');

            $localStorage.currentPay.userModal = 2;
        }

    });
