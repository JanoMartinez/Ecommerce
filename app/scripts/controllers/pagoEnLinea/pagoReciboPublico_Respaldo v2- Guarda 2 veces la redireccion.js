﻿'use strict';
angular.module('softvFrostApp')
    .controller('PagoReciboPublicoCtrl', function ($uibModal, $state, $rootScope, ngNotify, $localStorage, $window, pagoReciboFactory, globalService, $sce, $http) // servicio
    {
        var vm = this;
        var ContratoComp;
        var ContratoReal;
        var NombreCliente;

        vm.textoPantalla ='TEXTO DE PRUEBA';

        this.$onInit = function () {   
            
            // url de respuesta
            console.log('PAGO RECIBO PUBLICO');

            // TOMAMOS LA URL QUE REDIRECCIONA AL PAGO -- recibo PUBLICO 
            var url1 = ''; 
            url1 = window.location.href;

             // Revisamos las variables del navegador 
            var urlnavegador = window.location.href;
            var n = urlnavegador.indexOf("resultIndicator");
            var url = new URL(urlnavegador);  

            if (typeof $localStorage.currentUser == 'undefined' && $localStorage.currentUser == null) // usuario sin loguear
            {        
                console.log('SIN LOGUEAR, puede que se haya cerrado la sesión $localStorage.currentUser: ', $localStorage.currentUser);

                if (n == -1)  // No existe resultIndicator en URL    
                {               
                    ngNotify.set('SIN VARIABLES, REGRESAR AL LOGIN ', 'warning'); 
                    $state.go('login'); // REGRESAMOS AL LOGIN 
                }
                else  // INICIA GUARDAR PAGO  
                {                              
                    pagoReciboFactory.guardaRedireccionPublico(1, 0, 0, '---', urlnavegador).then(function (data) {
                        console.log('1 Guardar pago de sesion SIN LOGUEAR ***', data);
                    });
                    // TERMINA GUARDAR PAGO                  
                    limpiarUrl();   
                } // fin else                  
            }
            else  // INICIA USUARIO LOGUEADO 
            {
                console.log('* USUARIO LOGUEADO * $localStorage.currentUser.contrato: ', $localStorage.currentUser.contrato );
                // guarda para bitácora
                pagoReciboFactory.guardaRedireccion(11, $localStorage.currentUser.contrato, $localStorage.currentPay.clvSessionCobra, $localStorage.currentPay.idSession, url1).then(function (data) {
                    console.log('11.- GuardaRedireccion - hazReturnData', data);                    
                });  
                   
                if (n == -1) {
                // No existe resultIndicator en URL  
                    pagoReciboFactory.guardaRedireccion(2, $localStorage.currentUser.contrato, $localStorage.currentPay.clvSessionCobra, $localStorage.currentPay.idSession, urlnavegador).then(function (data) {
                           //    console.log('redireccion guardada ***',nivel,' - ', data);
                        console.log('2.- No existe *resultIndicator* pero SI guardamos la redireccion - hazReturnData', data);
                    });                                                           
                }
                else {
                        // INICIA GUARDAR PAGO 
                        // Redirección por pago, si tiene resultIndicator, se verifica el pago en la bd                        
                    console.log('1.- ***');
                    pagoReciboFactory.guardaRedireccion(1, $localStorage.currentUser.contrato, $localStorage.currentPay.clvSessionCobra, $localStorage.currentPay.idSession, urlnavegador).then(function (data) {
                        console.log('1.- Si tiene *resultIndicator* SI guardaRedireccion - hazReturnData', data );
                        if (data.GetGuarda_RedireccionResult == 2)
                        {
                            console.log('SÍ PODEMOS RECUPERAR LOS DATOS DEL PAGO: ');
                            var idSessionCobra = $localStorage.currentPay.clvSessionCobra; 
                            pagoReciboFactory.banamexRetrieveNuevo(idSessionCobra).then(function (data) {  
                                console.log('datos recuperados ', data );   
                            });//Fin exec retrieve
                        } 
                    }); 
                        // TERMINA GUARDAR PAGO
                } // fin guardar pago   

               //limpiarUrl(); descomentar  
               //setTimeout( function() { logOut()}, 5000); descomentar // Terminamos session de usuario logueado 
            }      // TERMINA USUARIO LOGUEADO 
        } 


        function logOut() {           
           delete $localStorage.currentUser;
           $window.location.reload();
        }

        function limpiarUrl(){            
            ngNotify.set('Limpiamos la url y regresamos al LOGIN ', 'success');
            setTimeout( function() { cambiarUrl()}, 5000);  
        }


        function cambiarUrl() {
            $localStorage.currentPay.urlCambiada = 1;
            //$state.go('home.recibo'); // REGRESAMOS 

            var urlResult = window.location.href;
            var afterComma = urlResult.substr(urlResult.indexOf("?")); // Contains 24 //     
            var quitarDeUrl = afterComma.substring(0, afterComma.indexOf("#"));
            location.href = location.href.replace(quitarDeUrl, '');
      

            $localStorage.currentPay.userModal = 2;
        }

    });
