'use strict';
angular.module('softvFrostApp')
	.factory('pagoReciboFactory', function($http, $q,  $base64, $window, globalService, $localStorage) {
		
		var factory = {};		
		var paths = {  					
			datosMerchant: '/Banamex/GetDatosMerchant',
			banamexRetrieve: '/Banamex/GetRetrieve',	
			banamexRetrieveNuevo: '/Banamex/GetRetrieveNuevo',				
			guardaPagoEnLinea: '/Banamex/GetGuardaPagoEnLinea',
			guardaReturnData: '/Banamex/GetGuarda_ReturnData',
			getIdSessionTransaccion: '/Banamex/GetIdSessionTransaccion',
			guardaRedireccion: '/Banamex/GetGuarda_Redireccion',
			guardaRedireccionPublico: '/freeAuth/GetGuarda_RedireccionPublico'
		};

		factory.getIdSessionTransaccion = function ( contrato, resultIndicator) {
			
			var deferred = $q.defer();
			var Parametros = {
				'contrato': contrato,
				'resultIndicator': resultIndicator
			};
			var config = {
				headers: {
					'Authorization': $localStorage.currentUser.token
				}
			};
			$http.post(globalService.getUrl() + paths.getIdSessionTransaccion, JSON.stringify(Parametros), config).then(function (response) {
				deferred.resolve(response.data);
			}).catch(function (response) {
				deferred.reject(response);
			});

			return deferred.promise;
		};







	    factory.datosMerchant = function () {
	      var deferred = $q.defer();
	      var config = {
	        headers: {
	          'Authorization': $localStorage.currentUser.token
	        }
	      };
	      $http.get(globalService.getUrl() + paths.datosMerchant, config).then(function (response) {
	        deferred.resolve(response.data);
	      }).catch(function (data) {
	        deferred.reject(data);
	      });
	      return deferred.promise;
	    };


		factory.banamexRetrieve = function (password, userid, sessionId, merchantId, clvsession) {
		
			var token = $base64.encode(userid + ':' + password);

			var deferred = $q.defer();
			var Parametros = {
				'idInt':1234,
				'merchantId' : merchantId,
				'sessionId' : sessionId,				
				'token': token,
				'clv_session':clvsession	
	            //,'returnUrl':  'http://localhost:9000/#!/home/pagoEnLinea/pago'                 
			};
			var config = {
				headers: {
					'Authorization': $localStorage.currentUser.token
				}
			};
			$http.post(globalService.getUrl() + paths.banamexRetrieve, JSON.stringify(Parametros), config).then(function (response) {
				deferred.resolve(response.data);
			}).catch(function (response) {
				deferred.reject(response);
			});

			return deferred.promise;
		};



		//factory.banamexRetrieveNuevo = function (password, userid, sessionId, merchantId, clv_session) {
		factory.banamexRetrieveNuevo = function (clvsession) {
			//var token = $base64.encode(userid + ':' + password);

			console.log('en factory clv_session', clvsession);
			var deferred = $q.defer();
			var Parametros = {
				'idInt':1234,
				'clv_session': clvsession               
			};
			var config = {
				headers: {
					'Authorization': $localStorage.currentUser.token
				}
			};
			$http.post(globalService.getUrl() + paths.banamexRetrieveNuevo, JSON.stringify(Parametros), config).then(function (response) {
				deferred.resolve(response.data);
			}).catch(function (response) {
				deferred.reject(response);
			});

			return deferred.promise;
		};




		factory.guardaPagoEnLinea = function ( idSessionCobra, contrato) {
			
			var deferred = $q.defer();
			var Parametros = {
				'clv_Session': idSessionCobra,
				'contrato': contrato
			};
			var config = {
				headers: {
					'Authorization': $localStorage.currentUser.token
				}
			};
			$http.post(globalService.getUrl() + paths.guardaPagoEnLinea, JSON.stringify(Parametros), config).then(function (response) {
				deferred.resolve(response.data);
			}).catch(function (response) {
				deferred.reject(response);
			});

			return deferred.promise;
		};


		factory.guardaReturnData = function (conOsinError, clvSession, retrieveData, resultIndicator,amount, description,id, brand, transactionId) {			
			
			var deferred = $q.defer();
			var Parametros = {
				'clv_Session': clvSession,
				'retrieveData': retrieveData,
				'resultIndicator':resultIndicator,
				'amount':amount,
				'description': description,
				'id':id,
				'brand':brand,
				'transactionId':transactionId,
				'conOsinError':conOsinError
			};			
			var config = {
				headers: {
					'Authorization': $localStorage.currentUser.token
				}
			};
			$http.post(globalService.getUrl() + paths.guardaReturnData, JSON.stringify(Parametros), config).then(function (response) {
				deferred.resolve(response.data);
			}).catch(function (response) {
				deferred.reject(response);
			});

			return deferred.promise;
		};



		factory.guardaRedireccion = function (nivel, contrato, clvsession, sessionId, laUrl) {
			//console.log('en FACTORY REDIRECCION ', 'nivel', nivel,'contrato' ,contrato, 'clv_session',clv_session, 'sessionId',sessionId, 'laUrl',laUrl);
			var deferred = $q.defer();
			var Parametros = {
				'idInt': 123,
				'nivel': nivel,
				'contrato': contrato,
				'clv_session': clvsession,
				'sessionId': sessionId,
				'laUrl': laUrl //'una cadena'
			};
			var config = {
				headers: {
					'Authorization': $localStorage.currentUser.token
				}
			};
			$http.post(globalService.getUrl() + paths.guardaRedireccion, JSON.stringify(Parametros), config).then(function (response) {
				deferred.resolve(response.data);
			}).catch(function (response) {
				deferred.reject(response);
			});

			return deferred.promise;
		};

		factory.guardaRedireccionPublico = function (nivel, contrato, clvsession, sessionId, laUrl) {
			//console.log('en FACTORY REDIRECCION ', 'nivel', nivel,'contrato' ,contrato, 'clv_session',clv_session, 'sessionId',sessionId, 'laUrl',laUrl);
			var deferred = $q.defer();
			var Parametros = {
				'idInt': 123,
				'nivel': nivel,
				'contrato': contrato,
				'clv_session': clvsession,
				'sessionId': sessionId,
				'laUrl': laUrl //'una cadena'
			};
			var config = {
				headers: {
					'Authorization': '123','Unique':'custom'
				}
			};
			$http.post(globalService.getUrl() + paths.guardaRedireccion, JSON.stringify(Parametros), config).then(function (response) {
				deferred.resolve(response.data);
			}).catch(function (response) {
				deferred.reject(response);
			});

			return deferred.promise;
		};








		return factory;

	});