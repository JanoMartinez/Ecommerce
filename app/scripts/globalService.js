'use strict';
angular.module('softvFrostApp')
	.service('globalService', function () {
		var svc = {};

		svc.getUrl = function() {			
			//return 'http://localhost:64481/SoftvWCFService.svc';	
			//return 'http://192.168.50.122:8001/SoftvWCFService.svc';	
			//return 'http://172.16.126.44:2000/SoftvWCFService.svc';
			//return 'http://172.16.126.58:2010/SoftvWCFService.svc'; // privada
		    return 'https://realizarpago.stargroup.com.mx:2010/SoftvWCFService.svc'; //pública -- producción
		};	

	    svc.getUrlCorreo = function () {
	    	return 'https://estadodecuentaenlinea.stargroup.com.mx:5000/SoftvWCFService.svc'; 	     
	    };				

		return svc;
	});


